# Mensa Sano by LSP263 Team

Mensa Sano is a web application developed by LSP263 Team, which primary goal is to help people who need some mental health assistance ASAP.

## Why is Mensa Sano application useful?

Drastic changes in daily life over the past century are fueling the growing burden of chronic depression.
Depression is [disease of modernitiy](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3330161/). Severe cases of depression
can lead to suicide. 
To battle this modern and trending phenomenon our team has created web application which aims to offer immediate help and information to people
suffering from depression.


## Access

You can access Mensa Sano web application through the following link: 

[https://mensa-sano.herokuapp.com/home](https://mensa-sano.herokuapp.com/home).

## Usage

Currently Mensa Sano website is only available in Croatian language (Hrvatski).

Website is not meant to replace actual help provided by specialist (psychiatrists, psychologists, etc.) 

On the website you can find some useful help like: chatbot, chat, information and contact numbers for institutions who deal with mental illness.

Here are a few pictures of web application.

![Image1](images/image_1.JPG)

![Image2](images/image_2.JPG)

![Image3](images/image_3.JPG)

## Used frameworks and libraries

Frontend
- [React](https://reactjs.org/)
- [Bootstrap](https://getbootstrap.com/)

Backend 
- [Node.js](https://nodejs.org/en/)

Chatbot
- [DialogFlow](https://dialogflow.cloud.google.com/)

Deploy
- [Heroku](https://www.heroku.com/)


## Team members

Website was created by LSP263 team consisting of following members :

- [Marin Jurić](https://gitlab.com/Frootek), student, FER Zagreb
- [Slavko Boldin](https://gitlab.com/bestik64), student, FER Zagreb
- [Nikolina Jukić](https://gitlab.com/nikolina_jukic), student, FER Zagreb

## Project status, support and contributing

This project is no longer being supported.
However, we believe there is more potential in this web application and if you need support or wish to contribute to this
project, please contact team leader  at his email address : marin.juric@hotmail.com.

## License
This software is licensed under the [MIT License](LICENSE).